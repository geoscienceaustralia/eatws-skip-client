# Enable running :make from vim to populate the quickfix list with all linting errors.
check:
	pre-commit run -a
