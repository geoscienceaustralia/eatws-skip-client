import pytest

from eatws_skip_client import skip


@pytest.fixture(scope="class")
def clear_db():
    assert "gagempa" not in skip._base
    assert "eatws" not in skip._base
    skip.post("/api/_nuke_database")
    yield
