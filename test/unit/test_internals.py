from io import BytesIO, StringIO
from unittest import TestCase

from eatws_skip_client.cli import _resolve_product
from eatws_skip_client.client import DiskBlob
from eatws_skip_client.oo import ProductReference


def test_content_polymorphism():
    for content in (
        b"hello",
        "hello",
        StringIO("hello"),
        BytesIO(b"hello"),
    ):
        assert DiskBlob(content).read() == b"hello"


def test_resolve_product():
    # These should both not throw an exception AND pass the assertions
    # Usual method of calling
    assert _resolve_product("myeventid", "myname", "latest") == ProductReference(
        name="myname", event_id="myeventid", version=None, latest=True
    )
    assert _resolve_product("myeventid", "myname", 5) == ProductReference(
        name="myname", event_id="myeventid", version=5, latest=None
    )
    assert _resolve_product("myeventid", "myname", None) == ProductReference(
        name="myname", event_id="myeventid", version=None, latest=None
    )
    # Hierarchical method of calling
    assert _resolve_product(None, "myid/myproduct/latest", None) == ProductReference(
        name="myproduct", event_id="myid", version=None, latest=True
    )
    assert _resolve_product(None, "myid/myproduct/v3", None) == ProductReference(
        name="myproduct", event_id="myid", version=3, latest=None
    )
    assert _resolve_product(None, "myid/myproduct/", None) == ProductReference(
        name="myproduct", event_id="myid", version=None, latest=None
    )
    assert _resolve_product(
        None, "/api/events/myid/myproduct/v1", None
    ) == ProductReference(name="myproduct", event_id="myid", version=1, latest=None)
    assert _resolve_product(
        None, "api/events/myid/myproduct/v1", None
    ) == ProductReference(name="myproduct", event_id="myid", version=1, latest=None)
    assert _resolve_product(None, "myid/myproduct", None) == ProductReference(
        name="myproduct", event_id="myid", version=None, latest=None
    )
    # No event ID
    assert _resolve_product(None, "products/myproduct", None) == ProductReference(
        name="myproduct", event_id=None, version=None, latest=None
    )
    assert _resolve_product(None, "products/myproduct/v5", None) == ProductReference(
        name="myproduct", event_id=None, version=5, latest=None
    )


class TestResolveProductExceptions(TestCase):
    def test_resolve_product_exceptions(self):
        with self.assertRaises(ValueError):
            _resolve_product(None, "myid/myproduct/v1/extra_guff", None)
