import os
import sys

import pytest


@pytest.mark.skipif(sys.version_info < (3, 6), reason="requires python3.6 or higher")
def test_sc3decl_parse():
    from gzip import decompress

    from eatws_skip_client import sc3decl

    with open(
        os.path.join(os.path.dirname(__file__), "ga2020gyrdmf.xml.gz"), "rb"
    ) as fh:
        ep = sc3decl.parseEventParameters(decompress(fh.read()))
    assert ep.events[0].publicID == "ga2020gyrdmf"
    assert len(ep.picks) == 305
    assert len(ep.amplitudes) == 239
    o = ep.events[0].preferredOrigin
    assert len(o.arrivals) == 305
    assert o.methodID == "LOCSAT"
    assert 415.3 < o.depth.value < 415.4
    assert o.evaluationStatus == "reviewed"
