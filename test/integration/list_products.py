import pytest

from eatws_skip_client import skip


@pytest.fixture(scope="class")
def numbered_products(clear_db):
    skip.create_event(id="e")
    for event_id in (None, "e"):
        for i, tags in zip(
            range(10),
            [
                [],
                ["one"],
                ["two"],
                ["three"],
                ["two"],
                ["five"],
                ["two", "three"],
                ["seven"],
                ["two"],
                ["three"],
            ],
        ):
            skip.upload_product(
                event_id=event_id,
                product_name=f"{event_id or ''}{i}",
                product_type="text",
                mimetype="text/plain",
                title="A test product",
                content=str(i).encode(),
                metadata={"boo": "hoo"},
                tags=tags,
            )


@pytest.mark.usefixtures("numbered_products")
class TestProductListing:
    def test_list_products_event_id(self):
        ps = list(skip.list_products(event_id=None))
        assert all(p.event_id is None for p in ps)
        assert len(ps) == 10

        ps = list(skip.list_products(event_id="e"))
        assert all(p.event_id == "e" for p in ps)
        assert len(ps) == 10

        ps = list(skip.list_products())
        assert len(ps) == 20

    def test_list_products_unassociated(self):
        with pytest.warns(DeprecationWarning):
            ps = list(skip.list_products(unassociated=True))
        assert all(p.event_id is None for p in ps)
        assert len(ps) == 10

    def test_list_products_by_tag(self):
        def getnames(tag, event_id=None):
            return {x.name for x in skip.list_products(event_id=event_id, tag=tag)}

        assert getnames("two") == {"2", "4", "6", "8"}
        assert getnames("three") == {"3", "6", "9"}
        assert getnames("five") == {"5"}
        assert getnames("seven") == {"7"}
        assert getnames("foo") == set()
        assert getnames("two", "e") == {"e2", "e4", "e6", "e8"}
        assert getnames("three", "e") == {"e3", "e6", "e9"}
        assert getnames("five", "e") == {"e5"}
        assert getnames("seven", "e") == {"e7"}
        assert getnames("foo", "e") == set()

    def test_product_search(self):
        def getnames(search, event_id=None):
            return {
                x.name for x in skip.list_products(event_id=event_id, search=search)
            }

        assert getnames("two") == {"2", "4", "6", "8"}
        assert getnames("three") == {"3", "6", "9"}
        assert getnames("five") == {"5"}
        assert getnames("seven") == {"7"}
        assert getnames("foo") == set()
        assert getnames("two", "e") == {"e2", "e4", "e6", "e8"}
        assert getnames("three", "e") == {"e3", "e6", "e9"}
        assert getnames("five", "e") == {"e5"}
        assert getnames("seven", "e") == {"e7"}
        assert getnames("foo", "e") == set()
