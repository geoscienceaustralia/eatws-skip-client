from datetime import datetime
from shutil import copyfileobj
from tempfile import TemporaryFile

import pytest

from eatws_skip_client import skip
from eatws_skip_client.oo import ProductReference


class ByteRepeater(object):
    """File-like object that repeats the byte string *seed* *times* times."""

    def __init__(self, seed, times):
        self.seed = seed
        self.len = len(seed) * times
        self.n = 0

    def seek(self, pos):
        self.n = pos

    def read(self, bs=-1):
        N = len(self.seed)
        left = self.len - self.n
        if bs == -1 or bs > left:
            bs = left
        r = b""
        residue = self.n % N
        if residue:
            c = min(bs, N - residue)
            r += self.seed[residue : residue + c]
            bs -= c
        if bs:
            r += self.seed * (bs // N)
            residue = bs % N
            r += self.seed[:residue]
        self.n += len(r)
        return r

    def tell(self):
        return self.n


@pytest.mark.usefixtures("clear_db")
class TestBasicUpload:
    def test_health(self):
        assert skip.server_healthy

    def test_db_is_blank(self):
        # If this fails, you're probably somehow RUNNING THIS AGAINST A REAL SERVER.
        # DON'T do that.
        assert list(skip.list_events()) == []
        assert list(skip.list_products()) == []

    def test_create_event(self):
        e1 = skip.create_event(
            id="test2000abcdef",
            event_time=datetime(year=2000, month=1, day=2, hour=0, minute=0, second=0),
            latitude=-40,
            longitude=120,
            depth_km=0,
        )
        e2 = skip.get_event("test2000abcdef")
        assert e1.id == e2.id
        assert e1.created_time is not None
        assert e2.created_time is not None

    def test_upload_product(self):
        p = skip.upload_product(
            event_id=None,
            product_name="test.txt",
            product_type="text",
            mimetype="text/plain",
            title="A test product",
            content=b"hullo there",
            metadata={"boo": "hoo"},
        )
        assert p == skip.get_product(event_id=None, product_name="test.txt")
        assert p.version == 1

    def test_upload_child(self):
        p = skip.upload_product(
            event_id=None,
            product_name="parent.txt",
            product_type="text",
            mimetype="text/plain",
            title="A parent product",
            content=b"hullo there",
            metadata={"boo": "hoo"},
        )
        p2 = skip.upload_product(
            event_id=None,
            product_name="child.txt",
            product_type="text",
            mimetype="text/plain",
            title="A child product",
            content=b"hullo there",
            parents=[p],
            metadata={"boo": "hoo"},
        )
        assert p.version == 1
        assert p2.version == 1

    def test_api_variations(self):
        p = skip.get_product(event_id=None, product_name="test.txt")
        assert p == skip.get_product(p)
        assert p == skip.get_product(ProductReference(name="test.txt"))
        assert p == skip.get_product(event_id=None, product_name="test.txt")

    def test_upload_another_version(self):
        p = skip.upload_product(
            event_id=None,
            product_name="test.txt",
            product_type="text",
            mimetype="text/plain",
            title="A test product",
            content=b"hullo again",
        )
        assert p == skip.get_product(event_id=None, product_name="test.txt")
        assert p.version == 2
        assert (
            skip.get_product(event_id=None, product_name="test.txt", version=2).version
            == 2
        )
        assert (
            skip.get_product(event_id=None, product_name="test.txt", version=1).version
            == 1
        )

    def test_upload_big_file(self):
        content = b"asdf" * 1000000
        p = skip.upload_product(
            event_id=None,
            product_name="big",
            product_type="text",
            mimetype="text/plain",
            title="A big text file",
            content=content,
        )
        assert p == skip.get_product(event_id=None, product_name="big")
        assert p.version == 1

    def test_upload_huge_stream(self):
        REPEATS = 100000000  # 100 million, for a 400MB file
        content = ByteRepeater(seed=b"asdf", times=REPEATS)
        p = skip.upload_product(
            event_id=None,
            product_name="huge",
            product_type="text",
            mimetype="text/plain",
            title="A huge text file",
            content=content,
        )
        assert p == skip.get_product(event_id=None, product_name="huge")
        assert p.version == 1
        assert p.filesize == 4 * REPEATS

        # test we can download without reading everything into memory
        with TemporaryFile() as tmpfile:
            copyfileobj(p.as_file(), tmpfile)
            assert tmpfile.tell() == p.filesize

    def test_upload_stub(self):
        skip.upload_product(
            event_id=None,
            product_name="stub.txt",
            product_type="text",
            mimetype="text/plain",
            title="A test product",
            content=None,
        )

    def test_upload_pending_version(self):
        p = skip.upload_product(
            event_id=None,
            product_name="test.txt",
            product_type="text",
            mimetype="text/plain",
            title="A test product",
            content=b"hullo again",
            review_status="pending",
        )
        current = skip.get_product(event_id=None, product_name="test.txt")
        assert p.version == 3
        assert current.version == 2

    def test_approve_version(self):
        p = skip.get_product(event_id=None, product_name="test.txt", version=3)
        copy = skip.get_product(event_id=None, product_name="test.txt", version=3)
        assert not p.current
        p.set_review_status("accepted")
        assert p.current
        assert not copy.current
        copy.refresh()
        assert copy.current

    def test_list_versions(self):
        p = skip.get_product(event_id=None, product_name="test.txt")
        vs = list(p.versions)
        assert len(vs) == 3
        assert {v.version for v in vs} == {1, 2, 3}

    def test_collection_page_size(self):
        for v in range(10):
            p = skip.upload_product(
                event_id=None,
                product_name="many.txt",
                product_type="text",
                mimetype="text/plain",
                title="A test product",
                content=b"%d" % v,
            )
        versions = list(p.versions)
        for pp in range(1, 11):
            iterator = skip.list_product_versions(p, per_page=pp)
            assert list(iterator) == versions
