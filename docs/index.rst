.. eatws-skip-client documentation master file, created by
   sphinx-quickstart on Fri Feb 21 03:08:21 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKIP Client Documentation
=========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

This python module provides a programmer-friendly interface to the SKIP.

.. currentmodule:: eatws_skip_client

.. autodata:: skip
.. autoclass:: SKIP
   :members:
.. autoclass:: Event
   :members:
.. autoclass:: Product
   :members:
.. autoclass:: ProductReference
   :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
