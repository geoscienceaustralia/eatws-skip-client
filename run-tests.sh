#!/bin/bash
set -e
cd "$(dirname $0)"
pip install -U pip
pip install -r requirements.txt
echo Waiting for SKIP server to be ready...
skip wait-until-healthy
echo Running tests.
pytest -vv test/unit/ test/integration/*.py "$@"
