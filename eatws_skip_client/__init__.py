"""The Seismic Knowledge Integration Platform client"""

import importlib.metadata

__version__ = importlib.metadata.version("eatws-skip-client")

from .client import SKIP, APIError
from .oo import Event, Product, ProductReference  # for type hints
from .sc3decl import parseEventParameters as parse_event_parameters

skip = SKIP()
"""A singleton instance of :class:`SKIP` initialized with no arguments; i.e.
pulling its configuration from the environment. See the documentation of
:class:`SKIP` for details on which environment variables to set."""

__all__ = [
    "APIError",
    "Event",
    "Product",
    "ProductReference",
    "SKIP",
    "__version__",
    "parse_event_parameters",
    "skip",
]
