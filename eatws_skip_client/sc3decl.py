"""Declarative parsing of SeisComP3 XML using declxml and a custom
type-annotation based class mapper.
"""

import re
from datetime import datetime
from functools import lru_cache
from typing import (
    BinaryIO,
    Generic,
    List,
    NoReturn,
    Optional,
    TextIO,
    TypeVar,
    Union,
    get_args,
    get_origin,
    get_type_hints,
)

from dateutil.parser import parse as parse_date

from . import declxml as xml

TextInput = Union[TextIO, BinaryIO, str, bytes]
T = TypeVar("T")


class Attribute(Generic[T]):
    pass


def _find(iterable, publicID):
    if publicID is not None:
        for x in iterable:
            if x.publicID == publicID:
                return x
    return None


def _after_parse(state, instance):
    # We hack in some references so that objects can find their parents, etc
    if instance is None or getattr(instance, "_empty", False):
        return None
    path = repr(state)
    pstate = state._processor_state
    if not hasattr(pstate, "_lookup"):
        pstate._lookup = {}
    pstate._lookup[path] = instance
    instance._path = path
    instance._lookup = pstate._lookup
    return instance


_hooks = xml.Hooks(after_parse=_after_parse)


def _unpack_optional(t):
    if get_origin(t) != Union:
        return False
    args = get_args(t)
    if len(args) == 2 and type(None) in args:
        return next(x for x in args if x != type(None))
    return False


def _field_from_annotation(name: str, t, **kwargs):
    if t in (None, type(None)):
        return None

    optional_type = _unpack_optional(t)
    if optional_type:
        kwargs["required"] = False
        kwargs["default"] = None
        return _field_from_annotation(name, optional_type, **kwargs)

    if get_origin(t) == list:
        try:
            st = get_args(t)[0]
        except IndexError:
            st = str
        # We want all arrays to be optional, i.e. length=0 is allowed
        kwargs["required"] = False
        if issubclass(st, Element):
            # Arrays of elements should be identified using their _tagName
            path = None
        else:
            # Arrays of primitive values should be identified using the alias provided
            path = name
        return xml.array(_field_from_annotation(path, st, **kwargs), name)

    if get_origin(t) == Attribute:
        kwargs.pop("default", None)
        return _field_from_annotation(".", get_args(t)[0], attribute=name, **kwargs)

    if issubclass(t, Element):
        kwargs.pop("default", None)
        return t._processor(name, **kwargs)

    return _processors[t](name, **kwargs)


class ElementMeta(type):
    def __init__(cls, *args):
        super().__init__(*args)

        cls._fields = []

        for k, v in get_type_hints(cls).items():
            f = _field_from_annotation(k, v)
            if f is not None:
                cls._fields.append(f)

    @property
    def _tag_name(cls):
        try:
            return cls._tagName
        except AttributeError:
            n = cls.__name__
            return n[0].lower() + n[1:]


class Element(metaclass=ElementMeta):
    def __init__(self, **data):
        self._empty = not data
        for k in self._fields:
            setattr(self, k.alias, data.get(k.alias))

    def asdict(self, recursive=False):
        if recursive:

            def tf(x):
                if isinstance(x, Element):
                    return x.asdict(recursive=True)
                if isinstance(x, list):
                    return [tf(y) for y in x]
                return x

        else:
            tf = lambda x: x
        return {k: tf(getattr(self, k)) for f in self._fields for k in [f.alias]}

    def _short_repr(self, x, key=None):
        if isinstance(x, list):
            try:
                noun = get_type_hints(type(self))[key].__args__[0].__name__
            except Exception:
                noun = "element"
            return f"[...{len(x)} {noun}s...]" if x else "[]"
        if isinstance(x, dict):
            return f"dict(...{len(x)} pairs...)" if x else "{}"
        return repr(x)

    def __repr__(self):
        INDENT = " " * 4
        try:
            name = self.publicID
        except AttributeError:
            name = type(self).__name__
        data = "\n".join(
            [
                INDENT + k + ": " + self._short_repr(v, k).replace("\n", "\n" + INDENT)
                for k, v in self.asdict().items()
                if k != "publicID" and v is not None and v != []
            ]
        )
        return f"{name} {{\n{data}\n}}"

    @classmethod
    @lru_cache()
    def _processor(cls, path=None, **kwargs):
        if path is None:
            path = cls._tag_name
        return xml.user_object(path, cls, cls._fields, **kwargs, hooks=_hooks)

    @classmethod
    def _parse(cls, data: TextInput, **kwargs):
        try:
            data = data.read()
        except AttributeError:
            pass
        if isinstance(data, bytes):
            data = data.decode()
        return xml.parse_from_string(cls._processor(**kwargs), data)

    def _serialize(self, **kwargs):
        return xml.serialize_to_string(type(self)._processor(), self, **kwargs)

    @property
    def parent(self):
        parent_path = re.sub(r"/[^/]+$", "", self._path)
        return self._lookup[parent_path]

    @property
    def root(self):
        return self._lookup["seiscomp/EventParameters"]


def _datetime(name, **kwargs):
    return xml.string(
        name,
        **kwargs,
        hooks=xml.Hooks(
            after_parse=lambda _, x: parse_date(x) if x else None,
            before_serialize=lambda _, x: None
            if x is None
            else x.strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        ),
    )


_processors = {
    str: xml.string,
    float: xml.floating_point,
    int: xml.integer,
    bool: xml.boolean,
    datetime: _datetime,
}


class CreationInfo(Element):
    agencyID: Optional[str]
    author: Optional[str]
    creationTime: Optional[datetime]
    modificationTime: Optional[datetime]
    version: Optional[str]


class Comment(Element):
    text: str
    id: Optional[str]

    creationInfo: Optional[CreationInfo]


class HasComments(Element):
    comments: List[Comment]


class HasCreationInfo(Element):
    creationInfo: Optional[CreationInfo]


class IDElement(Element):
    publicID: Attribute[str]


class MajorElement(IDElement, HasComments, HasCreationInfo):
    pass


class RealQuantity(Element):
    value: float
    uncertainty: Optional[float]
    lowerUncertainty: Optional[float]
    upperUncertainty: Optional[float]
    confidenceLevel: Optional[float]


class TimeQuantity(Element):
    value: datetime
    uncertainty: Optional[float]
    lowerUncertainty: Optional[float]
    upperUncertainty: Optional[float]
    confidenceLevel: Optional[float]


class TimeWindow(Element):
    reference: datetime
    begin: float
    end: float


class WaveformID(Element):
    networkCode: Attribute[str]
    stationCode: Attribute[str]
    locationCode: Optional[Attribute[str]]
    channelCode: Optional[Attribute[str]]

    def __repr__(self):
        return f"<WaveformID {self}>"

    def __str__(self):
        return ".".join(
            x or ""
            for x in [
                self.networkCode,
                self.stationCode,
                self.locationCode,
                self.channelCode,
            ]
        )


class Pick(MajorElement):
    publicID: Attribute[str]
    time: TimeQuantity
    phaseHint: Optional[str]
    evaluationMode: Optional[str]
    waveformID: Optional[WaveformID]


class Arrival(HasCreationInfo):
    pickID: str
    phase: str
    azimuth: Optional[float]
    distance: Optional[float]
    timeResidual: Optional[float]
    timeCorrection: Optional[float]
    timeUsed: Optional[bool]

    horizontalSlownessResidual: Optional[float]
    horizontalSlownessUsed: Optional[bool]
    backAzimuthResidual: Optional[float]
    backAzimuthUsed: Optional[bool]

    earthModelID: Optional[str]
    weight: Optional[float]

    origin = Element.parent

    @property
    def pick(self) -> Pick:
        return _find(self.root.picks, self.pickID)


class Amplitude(MajorElement):
    pickID: str
    type: str
    snr: Optional[float]
    amplitude: Optional[RealQuantity]
    period: Optional[RealQuantity]
    scalingTime: Optional[TimeQuantity]
    timeWindow: Optional[TimeWindow]
    waveformID: Optional[WaveformID]
    evaluationMode: Optional[str]

    @property
    def pick(self) -> Pick:
        ep = self.parent
        return _find(ep.picks, self.pickID)


class StationMagnitude(MajorElement):
    magnitude: RealQuantity
    type: Optional[str]
    passedQC: Optional[bool]
    amplitudeID: Optional[str]
    methodID: Optional[str]
    waveformID: Optional[WaveformID]

    origin = Element.parent

    @property
    def amplitude(self) -> Amplitude:
        return _find(self.root.amplitudes, self.amplitudeID)


class StationMagnitudeContribution(Element):
    stationMagnitudeID: str
    residual: Optional[float]
    weight: Optional[float]

    magnitude = Element.parent

    @property
    def stationMagnitude(self) -> StationMagnitude:
        return _find(self.magnitude.origin.stationMagnitudes, self.stationMagnitudeID)


class Magnitude(MajorElement):
    magnitude: RealQuantity
    type: Optional[str]
    stationCount: Optional[int]
    methodID: Optional[str]
    azimuthalGap: Optional[float]
    evaluationStatus: Optional[str]
    contributions: List[StationMagnitudeContribution]

    origin = Element.parent


class OriginQuality(Element):
    associatedPhaseCount: Optional[int]
    usedPhaseCount: Optional[int]
    depthPhaseCount: Optional[int]
    associatedStationCount: Optional[int]
    usedStationCount: Optional[int]
    standardError: Optional[float]
    azimuthalGap: Optional[float]
    secondaryAzimuthalGap: Optional[float]
    minimumDistance: Optional[float]
    maximumDistance: Optional[float]
    medianDistance: Optional[float]
    groundTruthLevel: Optional[str]

    origin = Element.parent


class Origin(MajorElement):
    publicID: Attribute[str]
    latitude: RealQuantity
    longitude: RealQuantity
    time: TimeQuantity
    depth: Optional[RealQuantity]
    depthType: Optional[str]
    type: Optional[str]

    timeFixed: Optional[bool]
    epicenterFixed: Optional[bool]
    methodID: Optional[str]
    earthModelID: Optional[str]
    evaluationMode: Optional[str]
    evaluationStatus: Optional[str]

    quality: Optional[OriginQuality]
    magnitudes: List[Magnitude]
    arrivals: List[Arrival]
    stationMagnitudes: List[StationMagnitude]


class NodalPlane(Element):
    strike: RealQuantity
    dip: RealQuantity
    rake: RealQuantity


class NodalPlanes(Element):
    nodalPlane1: Optional[NodalPlane]
    nodalPlane2: Optional[NodalPlane]
    preferredPlane: Optional[int]

    def preferredNodalPlane(self) -> Optional[NodalPlane]:
        if self.preferredPlane == 1:
            return self.nodalPlane1
        if self.preferredPlane == 2:
            return self.nodalPlane2
        return None


class Axis(Element):
    azimuth: RealQuantity
    plunge: RealQuantity
    length: RealQuantity


class PrincipalAxes(Element):
    tAxis: Axis
    pAxis: Axis
    nAxis: Optional[Axis]


class SymmetricTensor(Element):
    Mrr: RealQuantity
    Mtt: RealQuantity
    Mpp: RealQuantity
    Mrt: RealQuantity
    Mrp: RealQuantity
    Mtp: RealQuantity


class MomentTensor(MajorElement):
    derivedOriginID: Optional[str]
    momentMagnitudeID: Optional[str]
    tensor: SymmetricTensor
    methodID: Optional[str]
    varianceReduction: Optional[float]
    doubleCouple: Optional[float]
    clvd: Optional[float]
    greensFunctionID: Optional[str]
    filterID: Optional[str]

    @property
    def derivedOrigin(self):
        return _find(self.root.origins, self.derivedOriginID)

    @property
    def momentMagnitude(self):
        return _find(self.root.magnitudes, self.momentMagnitudeID)


class FocalMechanism(MajorElement):
    triggeringOriginID: Optional[str]
    nodalPlanes: Optional[NodalPlanes]
    principalAxes: Optional[PrincipalAxes]
    momentTensor: Optional[MomentTensor]
    azimuthalGap: Optional[float]
    stationPolarityCount: Optional[float]
    misfit: Optional[float]
    stationDistributionRatio: Optional[float]
    methodID: Optional[str]

    @property
    def triggeringOrigin(self):
        return _find(self.root.origins, self.triggeringOriginID)


class EventDescription(Element):
    _tagName = "description"
    text: str
    type: str


class Event(MajorElement):
    preferredOriginID: Optional[str]
    preferredMagnitudeID: Optional[str]
    preferredFocalMechanismID: Optional[str]
    originReferences: List[str]
    focalMechanismReferences: List[str]

    type: Optional[str]
    typeCertainty: Optional[str]
    descriptions: List[EventDescription]

    @property
    def preferredOrigin(self) -> Optional[Origin]:
        return _find(self.root.origins, self.preferredOriginID)

    @property
    def preferredFocalMechanism(self) -> Optional[FocalMechanism]:
        return _find(self.root.focalMechanisms, self.preferredFocalMechanismID)

    @property
    def preferredMagnitude(self) -> Optional[Magnitude]:
        mid = self.preferredMagnitudeID
        if not mid:
            return None
        for o in self.root.origins:
            for m in o.magnitudes:
                if m.publicID == mid:
                    return m

    @property
    def origins(self) -> List[Origin]:
        refs = set(self.originReferences)
        if self.preferredOriginID:
            refs.add(self.preferredOriginID)
        return [_find(self.root.origins, oid) for oid in refs]

    @property
    def magnitudes(self) -> List[Magnitude]:
        return list(
            set(magnitude for origin in self.origins for magnitude in origin.magnitudes)
        )

    @property
    def focalMechanisms(self) -> List[FocalMechanism]:
        refs = set(self.focalMechanismReferences)
        if self.preferredFocalMechanismID:
            refs.add(self.preferredFocalMechanismID)
        return [_find(self.root.focalMechanisms, fmid) for fmid in refs]

    @property
    def regionName(self) -> Optional[str]:
        for d in self.descriptions:
            if d.type == "region name":
                return d.text
        return None


class EventParameters(HasComments, HasCreationInfo):
    _tagName = "seiscomp/EventParameters"
    origins: List[Origin]
    picks: List[Pick]
    amplitudes: List[Amplitude]
    focalMechanisms: List[FocalMechanism]
    events: List[Event]
    description: Optional[str]

    @property
    def event(self) -> Optional[Event]:
        if not self.events:
            return None
        if len(self.events) > 1:
            from warnings import warn

            warn("More than one event found! Returning just the first one.")
        return self.events[0]

    @property
    def parent(self) -> NoReturn:
        raise ValueError("Root element EventParameters has no parent.")

    def __dir__(self):
        return [x for x in super().__dir__() if x != "parent"]

    @property
    def magnitudes(self) -> List[Magnitude]:
        return list(
            set(magnitude for origin in self.origins for magnitude in origin.magnitudes)
        )


def parseEventParameters(document: TextInput) -> EventParameters:
    """Parse a seiscomp3 Event Parameters XML document into a user-friendly
    structure.

    :param document: a string or file-like object to parse."""
    return EventParameters._parse(document)


def serialize(element: Element, indent="  ", **kwargs) -> str:
    return element._serialize(indent=indent, **kwargs)


parse = EventParameters._parse
