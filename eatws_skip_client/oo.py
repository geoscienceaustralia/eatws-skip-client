"""
This module defines an object-oriented API in a similar style to boto3.resource.
For now, these resources are read-only, and must be created using the methods
on the SKIP class.
"""

from __future__ import annotations

import sys
from datetime import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Literal, Optional, Union

from pydantic import BaseModel, ConfigDict, model_validator

if TYPE_CHECKING:
    from eatws_skip_client.client import SKIP


class Resource(BaseModel):
    """A SKIP Resource. Just a pydantic model (built from JSON returned from
    the SKIP REST API) holding a reference to a SKIP client and with some
    convenience methods."""

    model_config = ConfigDict(extra="allow")
    _client: SKIP

    def __init__(self, _client: SKIP, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._client = _client

    def _label(self):
        return ""

    def __repr__(self):
        from pprint import pformat

        label = self._label()
        if label:
            label += " "
        data = pformat(self.model_dump())
        name = type(self).__name__
        return "{} {}{}".format(name, label, data)

    created_time: datetime
    updated_time: datetime


class ProductReference(BaseModel):
    """Data pointing to a product in the SKIP, either by specifying an exact
    version, or by asking for the latest or current version."""

    name: str
    event_id: Optional[str] = None
    version: Optional[int] = None
    latest: Optional[bool] = None

    @staticmethod
    def build(
        product: Union[Product, ProductReference, None] = None,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
    ) -> ProductReference:
        """Convenience constructor used to canonicalize arguments to various
        methods on the SKIP client class."""
        if isinstance(product, ProductReference):
            return product
        elif isinstance(product, Product):
            return product.ref
        elif product is not None:
            raise ValueError("product must be a ProductReference or Product")
        if product_name is None:
            raise ValueError("Must provide one of product or product_name")
        return ProductReference(
            name=product_name, event_id=event_id, version=version, latest=latest
        )

    @model_validator(mode="after")
    def validate_version_latest(self):
        if self.version is not None and self.latest:
            raise ValueError("Cannot specify version and latest")
        return self

    @property
    def key(self):
        k = self.event_id or "products"
        k += "/" + self.name
        if self.version:
            k += "/v{}".format(self.version)
        elif self.latest:
            k += "/latest"
        return k

    @property
    def product_name(self):
        return self.name

    def url(self, extra=""):
        event = self.event_id and "events/{}".format(self.event_id) or "products"
        if self.version:
            vbit = "/v{}".format(self.version)
        elif self.latest:
            vbit = "/latest"
        else:
            vbit = ""
        return "/api/{}/{}{}{}".format(event, self.product_name, vbit, extra)

    @property
    def cache_key(self):
        if not self.version:
            return None
        return (self.event_id, self.name, self.version)


ReviewStatus = Literal[
    "rejected",
    "pending",
    "not_required",
    "accepted",
    "inherit",
    "staging",
]


class Product(Resource):
    """A product stored in the SKIP. You should never instantiate this class
    directly - use :meth:`.SKIP.get_product` to get existing products or
    :meth:`.SKIP.upload_product` to create new ones."""

    event_id: Optional[str]
    name: str
    version: int
    product_type: str
    current: bool
    public: bool
    storage_type: str
    title: Optional[str]
    geography: Union[
        str, dict, None
    ]  # This can be arbitrary GeoJSON, or maybe WKT too?
    time: Optional[datetime]
    review_status: ReviewStatus
    computed_review_status: ReviewStatus
    filesize: Optional[int]
    related_event_ids: List[str]
    data: Dict[str, Any]
    tags: List[str]
    key: str

    def _label(self):
        return self.key

    @property
    def _identity(self):
        return dict(
            event_id=self.event_id, product_name=self.name, version=self.version
        )

    def __hash__(self):
        return hash("%s/%s/%i" % (self.event_id, self.name, self.version))

    @property
    def ref(self):
        """The :class:`ProductReference` pointing to this product."""
        # Since this a concrete product with all metadata retrieved,
        # we should refer to it by its exact version number.
        return ProductReference(
            name=self.name, event_id=self.event_id, version=self.version
        )

    @property
    def content(self):
        """Get the contents of this file.
        For large files, this will use lots of memory - consider using
        Product.stream() instead.

        :returns bytes: The file contents as a bytestring."""
        return self._client.get_product_content(self)

    @property
    def log(self):
        """Get the creation log of this product.

        :returns bytes: The log contents as a bytestring."""
        return self._client.get_product_log(self)

    @property
    def event(self):
        """Get the event that owns this product (if any)."""
        if self.event_id:
            return self._client.get_event(self.event_id)
        else:
            return None

    @property
    def is_stub(self):
        """True if this product is a stub; i.e. is missing its contents."""
        return self.storage_type == "stub"

    def as_file(self):
        """Access this product's content as a file-like object streaming from
        the remote server.

        :returns: A file-like object."""
        return self._client.get_product_as_file(self)

    def stream(self):
        """
        Stream the contents of this file.

        :returns: A generator that yield chunks of bytes.
        """
        return self._client.stream_product_content(self)

    def set_review_status(self, status):
        """Set the review status of this product.

        :param str status: One of the following review status values:
            "not_required", "pending", "accepted", "rejected", "inherit"
        """
        self._client.set_product_review_status(self, status=status)
        self.refresh()  # review status can cause other attributes to change on the backend, so we need to re-read

    def refresh(self):
        """Refresh this product from the remote server."""
        self._client._purge_product_cache(**self._identity)
        new = self._client.get_product(self)
        assert new is not None
        extra = new.model_extra or {}
        for field in (*new.model_fields_set, *extra):
            setattr(self, field, getattr(new, field))

    @property
    def parents(self):
        """Collection of parents of this product."""
        return self._client.list_product_parents(self)

    @property
    def children(self):
        """Collection of children of this product."""
        return self._client.list_product_children(self)

    @property
    def versions(self):
        """Collection of versions of this product."""
        return self._client.list_product_versions(self)


class Event(Resource):
    """A seismic event stored in the SKIP. You should never instantiate this
    class directly - use :meth:`eatws_skip_client.SKIP.get_event` to get
    existing events or :meth:`eatws_skip_client.SKIP.create_event` to create
    new ones."""

    id: str
    event_time: Optional[datetime]
    longitude: Optional[float] = None
    latitude: Optional[float] = None
    depth_km: Optional[float] = None
    meta_data: dict = {}
    rejected: bool
    event_type: Optional[str] = None
    event_type_certainty: Optional[str] = None
    origin_evaluation_mode: Optional[str] = None
    origin_evaluation_status: Optional[str] = None
    magnitude: Optional[float] = None
    magnitude_type: Optional[str] = None
    region_name: Optional[str] = None
    felt_report_count: int = 0

    def _label(self):
        return self.id

    def get_product(self, name, version=None, latest=False):
        """Get a product associated with this event."""
        return self._client.get_product(
            event_id=self.id, product_name=name, version=version, latest=latest
        )

    def list_products(self, **kwargs):
        """List the products associated with this event."""
        return self._client.list_products(event_id=self.id, **kwargs)

    @property
    def products(self):
        """The products associated with this event."""
        return self.list_products()

    @property
    def detailed_parameters(self):
        if sys.version_info < (3, 6):
            raise Exception(
                "You need python version 3.6+ to use "
                "the detailed event parameters functionality."
            )
        product = self.get_product("sc3_event_parameters.xml")
        if not product:
            return None
        from . import sc3decl

        return sc3decl.parseEventParameters(product.content)
