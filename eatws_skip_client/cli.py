#!/usr/bin/env python

import json
import os
import sys
from os.path import basename, getsize

import click
from pydantic import BaseModel
from tqdm import tqdm

from eatws_skip_client import SKIP, APIError, ProductReference


def env(var, default=""):
    return lambda: os.environ.get(var, default)


def env_bool(var, default=False):
    def get():
        x = os.environ.get(var, None)
        if x is None:
            return default
        x = x.strip().lower()
        return x in ["true", "yes", "y", "on", "1"]

    return get


def entry_point(*args, **kwargs):
    # Click fails to start in python3 if we don't have a nice utf-8 locale;
    # so we just override what the terminal tells us. Not great, not terrible.
    os.environ["LC_ALL"] = "en_US.utf-8"
    os.environ["LANG"] = "en_US.utf-8"
    try:
        cli(*args, **kwargs)
    except APIError as e:
        click.echo(e.msg, err=True)
        sys.exit(1)


@click.group()
@click.option(
    "--url",
    "-U",
    type=str,
    default=None,
    help="The base URL of the SKIP server, e.g. https://skip.eatws.net. "
    + "This option can also be specified in the environment variable SKIP_URL.",
)
@click.option(
    "--username",
    "-u",
    type=str,
    default=None,
    help="SKIP root username. "
    + "This option can also be specified in the environment variable SKIP_USERNAME.",
)
@click.option(
    "--password",
    "-P",
    type=str,
    default=None,
    help="SKIP root password. "
    + "This option can also be specified in the environment variable SKIP_PASSWORD.",
)
@click.option(
    "--api-key",
    "-k",
    type=str,
    default=None,
    help="SKIP API key. "
    + "This option can also be specified in the environment variable SKIP_API_KEY.",
)
@click.option(
    "--secret-id",
    "-s",
    type=str,
    default=None,
    help="Secret ID in AWS Secrets Manager in which SKIP credentials are stored. "
    + "This option can also be specified in the environment variable SKIP_SECRET_ID.",
)
@click.option(
    "--source",
    type=str,
    default="{}",
    help="JSON object containing source/provenance information"
    + "This option can also be specified in the environment variable SKIP_SOURC.",
)
@click.option(
    "--pretty/--ugly",
    default=env_bool("SKIP_PRETTY", False),
    help="Should JSON outputs be pretty-printed? "
    + "This option can also be specified in the environment variable SKIP_PRETTY.",
)
@click.option("--debug/--no-debug", help="Print details of HTTP requests")
def cli(url, source, debug, pretty, **auth_params):
    """A simple CLI client for the SKIP."""
    global skip, _pretty
    if debug:
        enable_debug_log()
    skip = SKIP(api_url=url, source=json.loads(source), **auth_params)
    _pretty = pretty


@cli.command()
def bash_utils():
    """Shows how to import the bash helper functions"""
    from os.path import abspath, dirname, join

    path = join(dirname(abspath(__file__)), "skip_bash.sh")
    print('# Either eval "$(skip bash-utils)", or run: \nsource ' + path)


@cli.group()
def event():
    """Commands for retrieving and manipulating events"""
    pass


@event.command()
@click.argument("id")
@click.option("--event-time", help="Time of event")
@click.option("--longitude", type=float, help="Longitude of event epicentre")
@click.option("--latitude", type=float, help="Latitude of event epicentre")
@click.option(
    "--depth-km", type=float, default=None, help="Depth in km of event epicentre"
)
@click.option("--meta-data", default="{}", help="Extra data as JSON")
def create(id, meta_data, **kwargs):
    """Create or update a seismic event."""
    kwargs["meta_data"] = json.loads(meta_data)
    skip.create_event(id, **kwargs)


@event.command(name="get")
@click.argument("id")
def get_event(id):
    """Get the metadata associated with a seismic event."""
    metadata = skip.get_event(id)
    if not metadata:
        raise APIError("Event not found")
    print(_to_json(metadata))


@event.command(name="list")
@click.option(
    "--sort",
    default="",
    help="A string describing the requested sort order, "
    + "with syntax as described in the SKIP API documentation.",
)
@click.option("--date-from", default="", help="Only return events after this date")
@click.option("--date-to", default="", help="Only return events before this date")
@click.option(
    "--filter",
    type=str,
    multiple=True,
    help="A string describing a filter, "
    + "with syntax as described in the SKIP API documentation. "
    + "This option can be repeated for multiple filters.",
)
@click.option(
    "--limit", help="The maximum number of events to return.", type=int, default=None
)
@click.option(
    "--array/--no-array",
    help="Should output be a JSON array, or newline-separated?",
    default=False,
)
@click.option(
    "--search", default=None, help="A string to search for in the event details"
)
def list_events(sort, date_from, date_to, filter, limit, array, search):
    """
    List seismic events.
    Each event is represented by a JSON object.
    By default, these JSON objects are each on a single line, with the newline
    serving as a record separator, sometimes known as the ndjson format.
    This is convenient for streaming, e.g. via the CLI tool jq.
    """
    _print_json_collection(
        skip.list_events(
            sort=sort,
            filters=filter,
            date_from=date_from,
            date_to=date_to,
            limit=limit,
            search=search,
        ),
        as_array=array,
    )


@cli.group()
def product():
    """Commands for retrieving and manipulating products"""
    pass


@product.command(name="list")
@click.option(
    "--event-id",
    "-E",
    type=str,
    default=None,
    help="Return products associated with this event",
)
@click.option(
    "--no-event",
    default=False,
    is_flag=True,
    help="Return products not associated with any event",
)
@click.option(
    "--sort",
    default="",
    help="A string describing the requested sort order, "
    + "with syntax as described in the SKIP API documentation.",
)
@click.option(
    "--filter",
    type=str,
    multiple=True,
    help="A string describing a filter, "
    + "with syntax as described in the SKIP API documentation. "
    + "This option can be repeated for multiple filters.",
)
@click.option(
    "--limit", help="The maximum number of events to return.", type=int, default=None
)
@click.option(
    "--array/--no-array",
    help="Should output be a JSON array, or newline-separated?",
    default=False,
)
def list_products(sort, event_id, no_event, filter, limit, array):
    """
    List products.
    Each event is represented by a JSON object.
    By default, these JSON objects are each on a single line, with the newline
    serving as a record separator, sometimes known as the ndjson format.
    This is convenient for streaming, e.g. via the CLI tool jq.
    """
    opts = dict(sort=sort, filters=filter, limit=limit)
    if no_event:
        opts["event_id"] = None
        if event_id is not None:
            raise ValueError("Can't use both --event-id and --no-event")
    elif event_id is not None:
        opts["event_id"] = event_id
    _print_json_collection(skip.list_products(**opts), as_array=array)


@product.command()
@click.option(
    "--event-id", "-E", type=str, default=None, help="Event ID to upload product under"
)
@click.option(
    "--name", "-n", type=str, default=None, help="Target filename for product"
)
@click.option("--title", "-m", type=str, help="Human-readable product title")
@click.option(
    "--product-type", "-t", type=str, default=None, help="Machine-readable product type"
)
@click.option(
    "--parents",
    "-p",
    type=str,
    default=None,
    help="Comma-separated list of parent products",
)
@click.option(
    "--tags", "-T", type=str, default=None, help="Comma-separated list of tags"
)
@click.option(
    "--logfile",
    "-L",
    type=str,
    default=None,
    help="File containing log of product creation",
)
@click.option(
    "--public/--private",
    "-a",
    default=False,
    help="Make product public (private is default)",
)
@click.option(
    "--review-status",
    default=None,
    help="Set product review status " + "(should be not_required, pending, or inherit)",
)
@click.option(
    "--source",
    type=str,
    default="{}",
    help="JSON object containing extra source/provenance information",
)
@click.argument("url")
def link(
    url,
    event_id,
    name,
    title,
    product_type,
    parents,
    tags,
    logfile,
    public,
    review_status,
    source,
):
    """
    Create a URL-type product.
    The argument URL specifies the link that should be stored in this product.
    """
    try:
        parents = parents.split(",")
    except Exception:
        parents = []

    try:
        tags = tags.split(",")
    except Exception:
        tags = []

    name = name or basename(url)

    skip.create_url(
        event_id=event_id,
        href=url,
        product_type=product_type,
        product_name=name,
        title=title,
        public=public,
        parents=parents,
        tags=tags,
        logfile=logfile,
        review_status=review_status,
        source=json.loads(source),
    )


@product.command()
@click.option(
    "--stub-if-missing/--error-if-missing",
    "--stub/--no-stub",
    "-S",
    help="Upload a stub if file is not provided or cannot be read",
    default=True,
)
@click.option(
    "--event-id", "-E", type=str, default=None, help="Event ID to upload product under"
)
@click.option(
    "--name",
    "-n",
    type=str,
    default=None,
    help="Target filename for product (default is input filename)",
)
@click.option("--title", "-m", type=str, help="Human-readable product title")
@click.option("--product-type", "-t", type=str, help="Machine-readable product type")
@click.option("--content-type", "--mimetype", "-C", type=str, help="MIME Type")
@click.option("--geography", type=str, help="Geographic information in EWKT format")
@click.option("--time", type=str, help="A time to associate with the product")
@click.option(
    "--parents",
    "-p",
    type=str,
    default=None,
    help="Comma-separated list of parent products",
)
@click.option(
    "--tags", "-T", type=str, default=None, help="Comma-separated list of tags"
)
@click.option(
    "--logfile",
    "-L",
    type=str,
    default=None,
    help="File containing log of product creation",
)
@click.option(
    "--public/--private",
    "-a",
    default=False,
    help="Make product public (private is default)",
)
@click.option(
    "--review-status",
    default=None,
    help="Set product review status " + "(should be not_required, pending, or inherit)",
)
@click.option(
    "--source",
    type=str,
    default="{}",
    help="JSON object containing extra source/provenance information",
)
@click.option(
    "--metadata", type=str, default="{}", help="JSON object containing extra metadata"
)
@click.option(
    "--stub-cause",
    type=str,
    default=None,
    help="If uploading a stub, a short message describing the "
    "reason for the product being missing.",
)
@click.option(
    "--make-unique/--replace-previous",
    default=False,
    help="Ensure product is unique - if another product with the "
    "same name and event assignment exists, modify the product name "
    "rather than replacing the previous version.",
)
@click.option(
    "--stream-upload/--form-upload",
    default=None,
    help="Stream upload (for small files, you can disable this for "
    "performance improvements).",
)
@click.option(
    "--extra-option",
    "-x",
    type=(str, str),
    multiple=True,
    help="Extra options to be passed to the SKIP HTTP API.",
)
@click.option("--no-progress-bar/--progress-bar", "-q", default=False)
@click.argument("filename")
def upload(
    event_id,
    name,
    filename,
    content_type,
    parents,
    tags,
    stub_if_missing,
    source,
    extra_option,
    metadata,
    make_unique,
    no_progress_bar,
    **kw,
):
    """
    Upload a file (or stub).
    """
    try:
        filesize = getsize(filename)
        content = open(filename, "rb")
    except Exception as e:
        filesize = None
        if stub_if_missing:
            print(
                "Warning: {} does not exist; sending stub.".format(filename),
                file=sys.stderr,
            )
            if not kw.get("stub_cause"):
                kw["stub_cause"] = "Attempted to upload non-existent file " + str(
                    filename
                )
            content = None
        else:
            raise e

    try:
        parents = parents.split(",")
    except Exception:
        parents = []

    try:
        tags = tags.split(",")
    except Exception:
        tags = []

    name = name or basename(filename)

    kw.update(dict(extra_option))

    # python2 closures can't modify nonlocal vars, so we have to use a dict:
    stats = dict(progress=2, bar=None)

    def callback(monitor):
        if not stats["bar"]:
            stats["bar"] = tqdm(total=filesize, unit="B", unit_scale=True, ascii=True)
        total_bytes = monitor.bytes_read
        stats["bar"].update(total_bytes - stats["progress"])
        stats["progress"] = total_bytes

    response = skip.upload_product(
        event_id=event_id,
        content=content,
        mimetype=content_type,
        product_name=name,
        parents=parents,
        tags=tags,
        source=json.loads(source),
        metadata=json.loads(metadata),
        callback=None if no_progress_bar else callback,
        filesize=filesize,
        **kw,
    )

    if stats["bar"]:
        stats["bar"].close()

    try:
        print(response._links["self"]["href"])
    except KeyError:
        sys.exit("Successfully uploaded product but could not determine URL.")


def _resolve_product(event_id, name, version):
    """
    Allow using "{event_id}/{product_name}/{version}" instead of CLI options
    Supports "[/][api][events]/event_id/product_name/[version]" or "[/][api][products]/product_name/[version]"
    Where items in square brackets are optional.
    version can be v[0-9]+ or "latest" or empty to get the 'current' version.
    """
    if not event_id:
        components = name.split("/")
        original_name = name
        components = components[1:] if components[0] == "" else components
        components = components[1:] if components[0] == "api" else components

        if components[0] == "products":
            _, name, *version = components
        elif components[0] == "events":
            _, event_id, name, *version = components
        else:
            event_id, name, *version = components

        if len(version) > 1:
            raise ValueError(f"Invalid product key format: {original_name}")

        version = version[0] if version else None
        version = version[1:] if version and version.startswith("v") else version

    if version == "latest":
        return ProductReference(name=name, event_id=event_id, latest=True)
    elif not version:  # Current, different from latest
        return ProductReference(name=name, event_id=event_id)
    else:
        return ProductReference(name=name, event_id=event_id, version=int(version))


@product.command()
@click.option("--event-id", "-E", default=None, help="Event ID")
@click.option("--version", "-v", default=None, help="Version of product to download")
@click.option("--output", "-o", default=None, help="Override name of downloaded file")
@click.option(
    "--stdout/--no-stdout",
    default=False,
    help="Write file contents to stdout instead of saving as a file",
)
@click.option("--no-progress-bar/--progress-bar", "-q", default=False)
@click.argument("name")
def download(name, event_id, version, output, stdout, no_progress_bar=False):
    """
    Downloads a product (of storage type File) to a local file.
    As an alternative to passing event ID and version as CLI options,
    you can instead request a product using the unique product key formats
        events/{event_id}/{product_name}/v{version_number}
        products/{product_name}/v{version_number}
    where the /v{} component is optional.
    An alternative to the /v{} component is "latest".
    """
    product = _resolve_product(event_id, name, version)

    if not output:
        output = product.product_name

    stream = skip.stream_product_content(product, chunk_size=4096)
    if not stream:
        meta = skip.get_product_metadata(product)
        if not meta:
            raise APIError("Product not found")
        else:
            raise APIError("Product has no content")

    progress = 0
    total = stream.total_bytes

    with sys.stdout if stdout else open(output, "wb") as fh:
        if not no_progress_bar:
            bar = tqdm(total=total, unit="B", unit_scale=True, ascii=True)
        for chunk in stream:
            progress += len(chunk)
            fh.write(chunk)
            if not no_progress_bar:
                bar.update(len(chunk))
    if not no_progress_bar:
        bar.close()
    if not stdout:
        print("{} written to {}".format(sizeof_fmt(progress), output))


@product.command(name="get")
@click.option("--event-id", "-E", default=None, help="Event ID")
@click.option("--version", "-v", default=None, help="Version of product to download")
@click.argument("name")
def get_product(name, event_id, version):
    """
    Gets the metadata associated with a product.
    As an alternative to passing event ID and version as CLI options,
    you can instead request a product using the unique product key formats
        events/{event_id}/{product_name}/v{version_number}
        products/{product_name}/v{version_number}
    where the /v{} component is optional.
    An alternative to the /v{} component is "latest".
    """
    product = _resolve_product(event_id, name, version)
    metadata = skip.get_product_metadata(product)
    if not metadata:
        sys.exit(1)
    print(_to_json(metadata))


@cli.command()
@click.option("--client", is_flag=True, help="Display the version of this SKIP client")
@click.option(
    "--server", is_flag=True, help="Display the version of the remote SKIP server"
)
def version(client, server):
    """Displays SKIP version information"""
    neither = not (client or server)
    if client or neither:
        print("SKIP client: v" + skip.client_version)
    if server or neither:
        if v := skip.server_version:
            print("SKIP server {}: {}".format(skip._base, v))
        elif skip.server_healthy:
            print(
                (
                    "SKIP server {} is healthy but could not report version; "
                    + "version is probably <v0.4."
                ).format(skip._base)
            )
        else:
            print(f"SKIP server {skip._base} appears unhealthy.")


@cli.command()
@click.argument("url")
def api_call(url):
    """GET any API endpoint."""
    print(_to_json(skip.get("/api/" + url).json()))


@cli.command()
@click.option("--event-id", "-E", default=None)
@click.option("--origin-id", "-O", default=None)
@click.option(
    "--array/--no-array",
    help="Should output be a JSON array, or newline-separated?",
    default=False,
)
def list_picks(event_id, origin_id, array):
    _print_json_collection(
        skip.list_picks(event_id=event_id, origin_id=origin_id), as_array=array
    )


@cli.command()
@click.option(
    "--max-tries", type=int, default=None, help="Maximum number of times to try"
)
def wait_until_healthy(max_tries=None):
    skip.wait_until_healthy(max_tries=max_tries)


def enable_debug_log():
    import logging

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


def _to_json(x):
    if isinstance(x, BaseModel):
        return x.model_dump_json(indent=(2 if _pretty else None))
    return json.dumps(x, indent=(2 if _pretty else None))


def _print_json_collection(iterator, as_array=False):
    """
    Print the json-encoding of each object in `iterator`.

    :param as_array: If false, objects are separated by newlines.
                     If true, objects are printed in one big JSON array.
    """
    first = True
    if as_array:
        print("[")
    for row in iterator:
        if not first:
            print("," if as_array else "")
        print(_to_json(row), end="")
        first = False
    print()
    if as_array:
        print("]")


def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, "Yi", suffix)
