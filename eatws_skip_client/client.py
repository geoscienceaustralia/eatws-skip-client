from __future__ import annotations

import json
import os
import sys
import time
import warnings
from collections import defaultdict
from datetime import datetime
from enum import Enum
from io import BytesIO, StringIO
from mimetypes import add_type, guess_type
from os.path import basename
from shutil import copyfileobj
from socket import gethostname
from tempfile import NamedTemporaryFile
from typing import (
    TYPE_CHECKING,
    Any,
    BinaryIO,
    Callable,
    Dict,
    Generator,
    Generic,
    List,
    Optional,
    Tuple,
    Type,
    TypeVar,
    Union,
    overload,
)
from urllib.parse import parse_qs, urlencode, urlparse, urlunparse
from warnings import warn

from pytz import timezone

from . import __version__
from .exceptions import APIError
from .oo import Event, Product, Resource, ReviewStatus
from .oo import ProductReference as ProductRef

# Custom seismological mimetypes
add_type("application/vnd.fdsn.mseed", ".mseed")
add_type("application/vnd.fdsn.mseed", ".miniseed")
add_type("application/vnd.fdsn.seed", ".seed")

# If the user does not specify whether to use singlepart or streamed upload,
# we stream files that bigger than this many bytes:
MAX_SINGLEPART_UPLOAD = 2**22  # 4MiB

try:
    from __main__ import __file__ as mainfile

    mainfile = basename(mainfile)
except Exception:
    mainfile = "unknown"

if TYPE_CHECKING:
    import requests

T = TypeVar("T")
R = TypeVar("R", bound=Resource)


class Unset(Enum):
    UNSET = 1


UNSET = Unset.UNSET


class DiskBlob(object):
    def __init__(self, content=None):
        if content is None:
            self.stub = True
        else:
            self.stub = False
            f = NamedTemporaryFile(delete=False)
            self.fname = f.name
            if isinstance(content, StringIO):
                content = content.read()
            if isinstance(content, str):
                try:
                    content = content.encode("utf-8")
                except UnicodeDecodeError:
                    pass
            if isinstance(content, bytes):
                f.write(content)
            else:
                copyfileobj(content, f)
            f.close()

    def read(self):
        if self.stub:
            return None
        with open(self.fname, "rb") as f:
            return f.read()

    def __del__(self):
        try:
            os.remove(self.fname)
        except Exception:
            pass


class SKIP(object):
    """A client providing an interface to a single SKIP server."""

    _disable_auth = False
    client_version = __version__

    def __init__(
        self,
        api_url=None,
        secret_id=None,
        api_key=None,
        username=None,
        password=None,
        source=None,
        cache=True,
        proxies=None,
        disable_auth=False,
    ):
        """Currently, authentication is only supported for service accounts
        (that is, you cannot use GA SSO).  These credentials can be provided in
        a number of ways:

        - Retrieved from AWS secrets manager via the parameter `secret_id`.
        - Directly via the parameter `api_key`.
        - Directly via the parameters `username` and `password`, if connecting to a SKIP
          with basic auth enabled
        - Not at all via the flag `disable_auth`, if connecting to a SKIP with auth
          disabled

        If none of these parameter sets are provided, environment variables are
        then checked in the following order:

        - ``SKIP_SECRET_ID``
        - ``SKIP_API_KEY``
        - ``SKIP_USERNAME`` & ``SKIP_PASSWORD``

        If credentials can still not be found after checking the environment,
        the client attempts to proceed in unauthenticated read-only mode. (Note
        that as of time of writing, there are no SKIP servers running that
        actually allow this mode of access; so you'll need credentials of one
        form or another.)

        :param str api_url: The root URL of the SKIP server including the protocol,
           e.g. "https://skip.eatws.net"
        :param str secret_id: The AWS Secrets Manager secret ID in which the SKIP
           credentials are stored.
        :param str api_key: API key to use to authenticate.
        :param str username: Username to use for basic auth.
        :param str password: Password to use for basic auth.
        :param dict source: A dictionary containing any additional provenance
           information that should be attached to any products created by this
           client. The following keys will be automatically populated:

           - `script` contains the filename of the python script calling this
             library.
           - `hostname` contains the hostname of the current machine.
           - `lib` contains the name and version of this client library.

        :param bool cache: Determines whether products will be cached or
           not. Metadata is cached in memory, file contents in temporary
           files. Defaults to ``True``.
        :param dict proxies: If present, HTTP(S) proxy configuration to be passed
           through to :func:`requests.request`.
        """
        source = source or json.loads(os.environ.get("SKIP_SOURCE", "{}"))
        api_url = api_url or os.environ.get("SKIP_URL", "https://skip.eatws.net")

        self._proxies = proxies

        if not (secret_id or username or api_key):
            secret_id = os.environ.get("SKIP_SECRET_ID")
            api_key = os.environ.get("SKIP_API_KEY")
            username = os.environ.get("SKIP_USERNAME")
            password = os.environ.get("SKIP_PASSWORD")

        try:
            host = gethostname()
        except Exception:
            host = None

        self._cache_enabled = cache
        self._source = {
            "script": mainfile,
            "lib": "eatws_skip_client v" + __version__,
            "host": host,
        }
        self._source.update(source)
        self._base = api_url
        self._product_cache = defaultdict(dict)
        self._event_cache = {}
        if disable_auth:
            self._disable_auth = True
        elif secret_id:
            try:
                import boto3
            except ImportError:
                sys.exit(
                    "You must install the boto3 library to "
                    "authenticate via AWS secrets manager."
                )
            sm = boto3.session.Session().client(service_name="secretsmanager")
            secrets = json.loads(
                sm.get_secret_value(SecretId=secret_id)["SecretString"]
            )
            (u, p, k) = [secrets.get(x) for x in ("username", "password", "api_key")]
            if k:
                self._api_key = k
            else:
                self._username = u
                self._password = p
        elif api_key:
            self._api_key = api_key
        elif username and password:
            self._username = username
            self._password = password
        else:
            self._username = None

    @property
    def server_version(self):
        """The version string of the connected server.

        :type str:"""
        try:
            return self.request(
                "GET", "/api/version", none_for=None, allow_unauthed=True
            ).json()
        except Exception:
            return None

    @property
    def server_healthy(self):
        """Whether the server can be contacted and is reporting itself as healthy.

        :type bool:"""
        try:
            return self.request(
                "GET", "/api/healthy", none_for=None, allow_unauthed=True
            ).json()
        except Exception:
            return False

    @property
    def server_url(self):
        """The base URL of the connected SKIP server.

        :type str:"""
        return self._base

    def wait_until_healthy(self, max_tries=None):
        """Block until the connected SKIP server is reporting itself a
        healthy. Waits 1 second in between checks.

        :param int max_tries: The maximum number of times to check health
            before raising an exception. By default, will wait forever."""
        tries = 0
        while max_tries is None or tries < max_tries:
            tries += 1
            if self.server_healthy:
                return True
            time.sleep(1)
        raise APIError("Max tries exceeded in wait_until_healthy")

    @overload
    def request(
        self,
        method,
        endpoint,
        none_for: None,
        raise_for=lambda _: True,
        retries=3,
        allow_unauthed: bool = False,
        *args,
        **kwargs,
    ) -> requests.Response: ...

    @overload
    def request(
        self,
        method,
        endpoint,
        none_for: Callable[..., bool] = lambda status: status == 404,
        raise_for=lambda _: True,
        retries=3,
        allow_unauthed: bool = False,
        *args,
        **kwargs,
    ) -> Optional[requests.Response]: ...

    def request(
        self,
        method,
        endpoint,
        none_for: Optional[Callable[..., bool]] = lambda status: status == 404,
        raise_for=lambda _: True,
        retries=3,
        allow_unauthed: bool = False,
        *args,
        **kwargs,
    ) -> Optional[requests.Response]:
        import requests
        from requests.exceptions import HTTPError

        if "headers" not in kwargs:
            kwargs["headers"] = {}

        if hasattr(self, "_api_key"):
            kwargs["headers"]["x-api-key"] = self._api_key
        elif getattr(self, "_username", False):
            kwargs["auth"] = (self._username, self._password)
        elif not (self._disable_auth or allow_unauthed):
            raise APIError(
                "This request requires authentication but no credentials were supplied"
            )

        status = 0
        tries = 0
        retriable = lambda s: s > 500 or s == 408
        while True:
            # Loop to retry on possibly-temporary server errors, e.g.:
            #  * gateway error due to temporary outage
            #  * our transaction was cancelled due to deadlock
            tries += 1
            response = requests.request(
                method, self._base + endpoint, proxies=self._proxies, *args, **kwargs
            )
            status = response.status_code
            if tries >= retries or not retriable(status):
                break

        if none_for is not None and none_for(status):
            return None

        if raise_for(status):
            try:
                response.raise_for_status()
            except HTTPError:
                msg = "Error {} from server".format(status)
                if status < 500:  # user error
                    try:
                        text = response.json()["error"]
                    except Exception:
                        text = response.text
                    msg += ": {}".format(text)
                else:
                    msg += "."
                raise APIError(msg)
        return response

    def get(self, endpoint, *args, **kwargs):
        return self.request("GET", endpoint, *args, **kwargs)

    def post(self, endpoint, *args, **kwargs):
        return self.request("POST", endpoint, *args, **kwargs)

    def delete(self, endpoint, raise_for_404=False, *args, **kwargs):
        return self.request(
            "DELETE",
            endpoint,
            raise_for=lambda s: raise_for_404 or s != 404,
            *args,
            **kwargs,
        )

    def put(self, endpoint, *args, **kwargs):
        return self.request("PUT", endpoint, *args, **kwargs)

    def _build_obj(self, cls: Type[R], data: dict) -> R:
        return cls.model_validate({**data, "_client": self})

    def _build_product(self, data: dict) -> Product:
        return self._build_obj(Product, data)

    def _build_event(self, data: dict) -> Event:
        return self._build_obj(Event, data)

    def create_event(self, id, event_time=None, **kwargs) -> Event:
        """
        Creates a new event from the passed parameters.

        :param str id: Unique ID for event
        :param float latitude: Latitude in degrees
        :param float longitude: Longitude in degrees
        :param float depth_km: Depth in km
        :param dict meta_data: A dictionary containing any extra metadata
        """
        if event_time:
            if isinstance(event_time, datetime):
                event_time = event_time.isoformat()
            kwargs["event_time"] = event_time
        response = self.request(
            "PUT", "/api/events/{}".format(id), json=kwargs, none_for=None
        )
        return self._build_event(response.json())

    def create_url(self, event_id, product_name, href, title, *args, **kwargs):
        """
        Creates a URL product pointing to `href`.

        :rtype: :class:`.Product`
        """
        return self.upload_product(
            event_id=event_id,
            product_name=product_name,
            href=href,
            title=title,
            *args,
            **kwargs,
        )

    def upload_product(
        self,
        event_id,
        product_name,
        product_type,
        title,
        parents=None,
        content=None,
        mimetype=None,
        tags=[],
        overwrite=False,
        public=False,
        logfile=None,
        href=None,
        review_status=None,
        geography=None,
        time=None,
        metadata={},
        make_name_unique=False,
        filesize=None,
        callback=None,
        warn_on_guessing_mimetype=True,
        stream_upload=None,
        content_type=None,
        source={},
        **extra,
    ) -> Product:
        """
        Creates a product, either by uploading a file or by creating a Stub.

        :param str event_id: The event which should own this product, or `None`
            if this is not desired.
        :param str product_name: The product's name (think filename).
        :param str title: The product's human-readable title.
        :param str product_type: A string representing the general type of
            product.
        :param str mimetype: The MIME Content Type of the file being uploaded.
        :param str content_type: Synonym for `mimetype`.
        :param content: The file content to upload, either as a file-like object,
            a bytestring, or `None` to upload a Stub.
        :param logfile: The log file of the product creation process, either as
            a file-like object or a filename to read.
        :param list(str) tags: A list of strings with which to tag this product.
        :param bool public: Whether or not to make this product publicly
            available once it is approved.
        :param str geography: A WKT-format geography to make this product
            spatially discoverable.
        :param datetime time: A datetime to make this product temporally
            discoverable.
        :param dict source: Additional provenance metadata describing how and
            where this product was produced.
        :param dict metadata: Arbitrary extra metadata to associate with this
            product.
        :param bool stream_upload: Whether this product should be uploaded as a
            multi-stage stream rather than a single request. Turned on by
            default only for large files.
        :param bool make_name_unique: By default, uploading with a
            (name, event_id) pair that already exists in SKIP will result in
            creating a new version of this existing product. If this flag is
            set, the product's name will instead be changed to be unique.
        :param int filesize: The size of the content being uploaded in bytes.
            Usually this is determined automatically; but if you're providing
            a strange file-like object you might need to provide this
            yourself.
        :param bool warn_on_guessing_mimetype: If `mimetype` is not provided,
            by default we try to guess it based on the product name and raise
            a warning. If this flag is set, the warning is suppressed.
        :param Callable callback: If stream_upload is true, a function to be
            called regularly with progress updates. It will be passed an
            object with an attribute `bytes_read` describing how many bytes
            have been uploaded so far.

        :rtype: class:`.Product`
        """
        combined_source = self._source.copy()
        combined_source.update(source)

        files: Dict[str, Tuple[str, Any, str]] = {
            "source": ("source.json", json.dumps(combined_source), "application/json"),
        }

        if "make_unique" in extra:
            make_name_unique = extra.pop("make_unique")

        if content_type and not mimetype:
            mimetype = content_type

        if logfile:
            if isinstance(logfile, str):
                logfile = open(logfile, "rb")
            files["log"] = ("log.txt", logfile, "text/plain")

        def product_id(p):
            if isinstance(p, str):
                return p
            if isinstance(p, Product):
                return p.key
            try:
                return p.headers.get("Location")
            except Exception:
                return p.get("key")

        parents = set(map(product_id, parents or []))
        data = dict(
            content_type=mimetype,
            product_type=product_type,
            title=title,
            name=product_name,
            public="true" if public else "false",
            metadata=json.dumps(metadata),
            parents=",".join(parents),
            tags=",".join(tags),  # TODO sanitization on this end?
            **extra,
        )

        # If filesize was not passed explicitly, try to extract it from content
        if content and filesize is None:
            filesize = get_filesize(content)
        if filesize is not None:
            data["filesize"] = str(filesize)

        # If user didn't specify whether or not to stream the upload, choose
        # based on the filesize and the MAX_SINGLEPART_UPLOAD threshold.  If we
        # couldn't determine the filesize, we go with streaming to be safe.
        if stream_upload is None:
            if filesize is not None:
                stream_upload = filesize >= MAX_SINGLEPART_UPLOAD
            else:
                stream_upload = True
        if stream_upload and content is not None:
            data["defer_contents"] = "true"
        if href is not None:
            data["href"] = href
        if geography is not None:
            data["geography"] = geography
        if time is not None:
            if isinstance(time, datetime):
                if time.tzinfo:
                    time = time.astimezone(timezone("UTC"))
                time = time.isoformat()
            data["time"] = time
        if review_status is not None:
            data["review_status"] = review_status
        if make_name_unique:
            data["make_name_unique"] = "true"

        if not mimetype:
            mimetype, _ = guess_type(product_name, strict=False)
            if not mimetype:
                raise APIError(
                    "Mimetype was not provided and could not be "
                    "guessed from filename."
                )
            if warn_on_guessing_mimetype:
                msg = (
                    "Mimetype was not provided, so we guessed "
                    "{} based on the filename '{}'. Pass "
                    "warn_on_guessing_mimetype=False if you're fine "
                    "with this."
                )
                warn(msg.format(mimetype, product_name))
            data["mimetype"] = mimetype

        endpoint = "/api/events/{}/".format(event_id) if event_id else "/api/products/"
        data.update(files)
        retries = 3
        if not stream_upload and content is not None:
            # if not streaming, include content in first POST
            data["file"] = (product_name, content, mimetype)
            # Don't retry if we're including the data, because content might be a stream
            retries = 0

        from requests_toolbelt.multipart.encoder import MultipartEncoder

        m = MultipartEncoder(fields=data)
        headers = {"Content-Type": m.content_type, "Expect": "100-continue"}

        result = self.request(
            "POST", endpoint, data=m, headers=headers, none_for=None, retries=retries
        ).json()

        if stream_upload and content is not None:
            # if streaming, send content separately afterwards
            if callback and hasattr(content, "read"):
                data = FileMonitor(content, callback)
            else:
                data = content

            endpoint = "/api/{}/upload".format(result["key"])
            # Don't retry this because content might be a stream
            result = self.request(
                "POST",
                endpoint,
                data=data,
                retries=0,
                headers={"Content-Type": mimetype, "Expect": "100-continue"},
                none_for=None,
            ).json()

        if self._cache_enabled:
            for cache_key in (
                (event_id, product_name, result["version"]),
                (event_id, product_name, None),
            ):
                self._product_cache[cache_key] = dict(
                    metadata=result,
                    content=DiskBlob(content),
                )

        return self._build_product(result)

    def get_product(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        **kwargs,
    ) -> Optional[Product]:
        """Returns a :class:`.Product` instance, which can be treated as a dictionary
        (containing the product metadata) but also has a .content attribute
        to access the file (if applicable).

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        md = self.get_product_metadata(product, **kwargs)
        if not md:
            return None
        return self._build_product(md)

    def get_product_log(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        **__,
    ):
        """Get the creation log of a product.

        :returns bytes: the log contents"""
        product = ProductRef.build(product, event_id, product_name, version, latest)
        url = product.url("/log")
        res = self.get(url)
        return res and res.content

    def get_product_metadata(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        refresh=False,
        **__,
    ):
        """Retrieve the metadata for a product.
        Returns a dictionary containing the results of JSON-decoding the
        top-level API endpoint associated with the specified product.
        You probably want to use :meth:`SKIP.get_product` instead, which
        extends this dictionary to a more convenient :class:`.Product`.

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        if self._cache_enabled and product.cache_key:
            cache = self._product_cache[product.cache_key]

            if refresh or "metadata" not in cache:
                res = self.get(product.url())
                if not res:
                    return None
                cache["metadata"] = res.json()
            return cache["metadata"]
        else:
            res = self.get(product.url())
            if not res:
                return None
            return res.json()

    def get_product_as_file(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        **__,
    ):
        """Access the content of a product as a file-like object.

        :param product:
            A :class:`.Product` or :class:`.ProductReference` specifying the
            product to be returned.
        :param str product_name:
            If `product` is not provided, this argument together with
            `event_id` specifies the product to return.
        :param str event_id:
            If `product` is not provided, this argument together with
            `product_name` specifies the product to return.
        :param int version:
            If `product` is not provided, this specifies the version number of
            the product to return.  By default, the current version is
            returned.
        :param bool latest:
            If `product` and `version` are not provided and `latest` is true,
            the latest version of the product is returned.

        :returns:
            A file-like object containing the content of the product.  Reading
            from this file-like object will stream-download from the remote
            server."""
        product = ProductRef.build(product, event_id, product_name, version, latest)
        response = self.get(
            product.url("/download"), stream=True, params=dict(allow_redirect=True)
        )
        if not response:
            return None
        return response.raw

    def stream_product_content(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        chunk_size=None,
        **__,
    ):
        """
        Stream the contents of a file-like product.

        :param int chunk_size: If not `None`, specifies the number
            of bytes to (attempt to) return per chunk.

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.

        :returns: a generator that yields chunks of bytes.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        response = self.get(
            product.url("/download"), stream=True, params=dict(allow_redirect=True)
        )
        if not response:
            return None

        length = response.headers.get("Content-Length")
        if length is not None:
            length = int(length)
        return Stream(response.iter_content(chunk_size=chunk_size), length)

    def get_product_content(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        refresh=False,
        **__,
    ):
        """
        Retrieve the contents of a product for which this makes sense (a File or URL).

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.

        :returns bytes: the raw file contents.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        url = product.url("/download")

        if self._cache_enabled and product.cache_key:
            cache = self._product_cache[product.cache_key]
            if refresh or "content" not in cache:
                res = self.get(url)
                if not res:
                    return None
                cache["content"] = DiskBlob(res.content)
            return cache["content"].read()
        else:
            res = self.get(url)
            if not res:
                return None
            return res.content

    def set_product_review_status(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        status: ReviewStatus,
        **__,
    ) -> ReviewStatus:
        """
        Sets the review status of a product.

        :param str status: One of the following review status values:
            "not_required", "pending", "accepted", "rejected", "inherit"

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        res = self.post(product.url("/review_status"), data=status)
        if not res or res.json() is not True:
            raise APIError("Error setting review status.")
        return status

    def list_product_versions(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        per_page=None,
        **kw,
    ):
        """Iterate over the versions of a product.

        :param int per_page: The number of products to fetch per server
            request.

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        return self.iterate(
            "product_versions",
            product.url("/versions"),
            resource_type=Product,
            per_page=per_page,
            **kw,
        )

    def list_product_parents(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        per_page=None,
        **kw,
    ):
        """Iterate over the parents of a product.

        :returns: an iterator of :class:`.Product` instances.

        :param int per_page: The number of products to fetch per server
            request.

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.

        :returns: an iterator of :class:`.Product` instances.
        """
        product = ProductRef.build(product, event_id, product_name, version, latest)
        return self.iterate(
            "products",
            product.url("/parents"),
            resource_type=Product,
            per_page=per_page,
            **kw,
        )

    def list_product_children(
        self,
        product: Union[Product, ProductRef, None] = None,
        *,
        event_id: Optional[str] = None,
        product_name: Optional[str] = None,
        version: Optional[int] = None,
        latest: Optional[bool] = None,
        per_page=None,
        **kw,
    ):
        """Iterate over the children of a product.

        :param int per_page: The number of products to fetch per server
            request.

        :param product: A :class:`.Product` or :class:`.ProductReference`
            specifying the product to be returned.
        :param str product_name: If `product` is not provided, this argument
            together with `event_id` specifies the product to return.
        :param str event_id: If `product` is not provided, this argument
            together with `product_name` specifies the product to return.
        :param int version: If `product` is not provided, this specifies the
            version number of the product to return.  By default, the current
            version is returned.
        :param bool latest: If `product` and `version` are not provided and
            `latest` is true, the latest version of the product is returned.

        :returns: an iterator of :class:`.Product` instances."""
        product = ProductRef.build(product, event_id, product_name, version, latest)
        return self.iterate(
            "products",
            product.url("/children"),
            resource_type=Product,
            per_page=per_page,
            **kw,
        )

    def _purge_product_cache(self, event_id, product_name, version):
        self._product_cache.pop((event_id, product_name, version))

    def get_event(self, id: str) -> Optional[Event]:
        """
        Get the details of a single seismic event, specified by id.

        :returns: :class:`.Event`
        """
        if not isinstance(id, str) or len(id) == 0:
            raise ValueError("id must be a non-empty string")
        res = self.get("/api/events/{}".format(id))
        if not res:
            return None
        return self._build_event(res.json())

    _defaultParser = lambda x: x

    def iterate(
        self,
        collection,
        url,
        limit=None,
        parser: Callable[[dict], R] = _defaultParser,
        resource_type: Optional[Type[R]] = None,
        per_page=None,
        with_len=True,
    ) -> ResultSetIterator[R]:
        if resource_type and parser is not type(self)._defaultParser:
            raise ValueError
        if resource_type:
            parser = lambda d: self._build_obj(resource_type, d)

        def request(u):
            response = self.get(u)
            if response:
                return response.json()

        if per_page:
            url = update_qs(url, dict(count=per_page))

        cls = ResultSetSequence if with_len else ResultSetIterator
        return cls(request, collection, url, limit=limit, parser=parser)

    def list_events(
        self,
        sort: str = "",
        filters: List[str] = [],
        date_from: Union[datetime, str, None] = None,
        date_to: Union[datetime, str, None] = None,
        limit: Optional[int] = None,
        search: Optional[str] = None,
    ):
        """
        Get a collection of seismic events, optionally sorted and filtered.

        :param sort: A string describing the requested sort order, with syntax
            as described in the SKIP API documentation.
        :param date_from: Start date of events to return, either as a python
            datetime object or a string. (Use ISO8601.)
        :param date_to: End date of events to return, either as a python
            datetime object or a string. (Use ISO8601.)
        :param filters: A list of strings describing any additional filters,
            with syntax as described in the SKIP API documentation.
        :param limit:   If present, the maximum number of events to return.
        :param search: A string to search for

        Returns an iterator of decoded JSON objects.
        """
        filters = list(filters)
        if date_from:
            filters += ["event_time>" + _render_date(date_from)]
        if date_to:
            filters += ["event_time<" + _render_date(date_to)]
        params: Dict[str, Any] = {"keyset_paging": "true"}
        if filters:
            params["filter"] = filters
        if sort:
            params["sort"] = sort
        if search:
            params["search"] = search
        start = update_qs("/api/events/", params)
        return self.iterate(
            "events",
            start,
            limit=limit,
            parser=lambda x: self._build_obj(Event, x),
            with_len=False,
        )

    def list_products(
        self,
        sort: str = "",
        filters: Optional[List[str]] = None,
        event_id: Union[str, None, Unset] = UNSET,
        limit: Optional[int] = None,
        tag: Optional[str] = None,
        search: Optional[str] = None,
        unassociated: bool = False,
    ):
        """Get the metadata of a collection of products, optionally sorted and
        filtered.

        :param event_id: If present and a string, only return products
            associated with the event of the given id. If None, only return
            unassociated products. If unset, products are returned regardless
            of their event association.
        :param sort: A string describing the requested sort order, with syntax
            as described in the SKIP API documentation.
        :param filters: A string describing any additional filters, with syntax
            as described in the SKIP API documentation.
        :param limit: If present, the maximum number of products to return.
        :param tag: If present, only return products with this tag.
        :param search: If present, only return products matching this search term.
        :param unassociated: If True, only return products not owned by any
            event. DEPRECATED - use event_id=None instead.

        Returns an iterator of decoded JSON objects."""
        filters = list(filters or [])
        params = {}

        if sort:
            params["sort"] = sort
        if search:
            params["search"] = search
        if tag:
            params["tag"] = tag

        if unassociated:
            if isinstance(event_id, str):
                raise APIError("Only one of unassociated and event_id can be supplied.")
            warnings.warn(
                "unassociated=True is deprecated - please use event_id=None instead.",
                DeprecationWarning,
            )
            event_id = None

        if isinstance(event_id, str):
            start_url = "/api/events/{}/".format(event_id)
        else:
            start_url = "/api/products/"

        if event_id is None:
            filters.append("!event_id?")

        if filters:
            params["filter"] = filters

        start_url = update_qs(start_url, params)
        return self.iterate(
            "products", start_url, limit=limit, parser=self._build_product
        )

    def list_picks(self, event_id=None, origin_id=None) -> ResultSetSequence[dict]:
        if bool(event_id) == bool(origin_id):
            raise ValueError("Must provide exactly one of event_id or origin_id")
        if event_id:
            r = self.request("GET", "/api/seiscomp/%s" % event_id, none_for=None)
            href = r.json()["_links"]["preferredOrigin"]["href"]
        else:
            href = "/api/seiscomp/%s" % origin_id
        return self.iterate("picks", href + "/picks")  # type: ignore


def _render_date(d):
    try:
        return d.isoformat()
    except Exception:
        return str(d)


class Stream(object):
    def __init__(self, generator, length=None):
        self._gen = generator
        self._len = length

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._gen)

    next = __next__

    @property
    def total_bytes(self):
        return self._len


class ResultSetIterator(Generic[T]):
    """
    Iterates over an API resultset using HAL links.
    Handles paging for you.

    :param request: A function that takes a URL and returns the corresponding
                    API response.
    :param key:     The key corresponding to the desired HAL collection.
    :param start:   The API URL to begin at.
    :param limit:   If present, the maximum number of results to return.
    """

    _limit: Optional[int]
    _key: str
    _results: list
    _count: int
    _pagesize: int
    _parser: Callable[[dict], T]

    def __init__(
        self,
        request,
        key,
        start,
        limit=None,
        pagesize=10,
        parser: Callable[[dict], T] = lambda x: x,
    ):
        self._request = request
        self._next = start
        self._key = key
        self._results = []
        self._limit = limit
        self._count = 0
        self._started = False
        self._pagesize = pagesize
        self._parser = parser
        self._total = None

    def __iter__(self):
        return self

    def nextpage(self):
        self._started = True
        if (
            self._pagesize
            and self._limit
            and self._count + self._pagesize > self._limit
        ):
            lastcount = self._limit - self._count
            self._next = update_qs(self._next, {"count": lastcount})
        response = self._request(self._next)

        if response:
            try:
                self._next = response["_links"]["next"]["href"]
            except (KeyError, TypeError):
                self._next = None

            try:
                self._results = response["_embedded"][self._key]
            except (KeyError, TypeError):
                self._results = []

            if not self._pagesize:
                self._pagesize = int(response.get("count"))

            if not self._total:
                tot = response.get("total")
                if tot is not None:
                    self._total = int(tot)
        else:
            self._next = None
            self._results = []

    def __next__(self):
        if self._limit is not None and self._count >= self._limit:
            raise StopIteration
        if not len(self._results):
            # We're out of results; get a new page
            if not self._next:
                # We were already at the last page; so stop iterating
                raise StopIteration
            self.nextpage()
        self._count += 1
        try:
            return self._parser(self._results.pop(0))
        except IndexError:
            raise StopIteration

    next = __next__


class ResultSetSequence(ResultSetIterator[T]):
    """ResultSetIterator with known length"""

    _total: int

    def __len__(self):
        if not self._started:
            self.nextpage()
        if self._limit is None:
            return self._total
        return min(self._total, self._limit)


def update_qs(url, changes):
    """
    Add or replace query string parameters in `url` as described by the
    dictionary `changes`.
    """
    parts = urlparse(url)
    qdict = parse_qs(parts.query)
    for k, v in changes.items():
        # If v is an iterable (that's NOT a [byte]string), pass it untouched.
        # Otherwise, wrap it in a list.
        if isinstance(v, (str, bytes)):
            v = [v]
        else:
            try:
                v = list(v)
            except TypeError:
                v = [v]
        qdict[k] = v
    return urlunparse(parts._replace(query=urlencode(qdict, doseq=True)))


_CHUNK_SIZE = 2**16  # 64KiB


class FileMonitor(object):
    """Wraps a file-like object to allow monitoring reads
    via a callback."""

    def __init__(self, fileobj: BinaryIO, callback=None):
        self.file = fileobj
        self.callback = callback
        self.bytes_read = 0

    def read(self, size=-1) -> bytes:
        string = self.file.read(size)
        self.bytes_read += len(string)
        if self.callback:
            self.callback(self)
        return string

    def __iter__(self) -> Generator[bytes, None, None]:
        while True:
            chunk = self.read(_CHUNK_SIZE)
            yield chunk
            if len(chunk) < _CHUNK_SIZE:
                break

    def __getattr__(self, name):
        return getattr(self.file, name)


def get_filesize(fh):
    """Given a file-like object or bytestring, try to determine its content
    length."""
    if isinstance(fh, BytesIO):
        return len(fh.getvalue())
    if isinstance(fh, bytes):
        return len(fh)
    if isinstance(fh, str):
        return len(fh.encode("utf-8"))
    try:
        return os.fstat(fh.fileno()).st_size
    except Exception:
        pass
