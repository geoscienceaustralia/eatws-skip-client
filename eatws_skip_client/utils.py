"""Utilities that are frequently useful when creating SKIP products"""

import logging
import os
import sys
from contextlib import contextmanager


@contextmanager
def noop():
    """No-op contextmanager - useful when using a context manager
    conditionally."""
    yield


# backwards-compatible stdout+stderr capturing
# (since many product builders might call external programs, we need this to
# get robust log capturing.)
# https://stackoverflow.com/a/22434262/9232196
@contextmanager
def redirect_fd(source=None, target=os.devnull):
    """Context manager that redirects one file descriptor to another for its
    duration."""
    if source is None:
        source = sys.stdout

    def fileno(file_or_fd):
        fd = getattr(file_or_fd, "fileno", lambda: file_or_fd)()
        if not isinstance(fd, int):
            raise ValueError("Expected a file (`.fileno()`) or a file descriptor")
        return fd

    stdout_fd = fileno(source)
    # copy stdout_fd before it is overwritten
    with os.fdopen(os.dup(stdout_fd), "wb") as copied:
        source.flush()  # flush library buffers that dup2 knows nothing about
        os.dup2(fileno(target), stdout_fd)  # $ exec >&target
        try:
            yield source  # allow code target be run with the redirected stdout
        finally:
            # restore stdout target its previous value
            source.flush()
            os.dup2(copied.fileno(), stdout_fd)  # $ exec >&copied


@contextmanager
def redirect_all_output(target=os.devnull):  # &>
    """Context manager that redirects all output to stdout/stderr to the
    specified stream for its duration."""
    with redirect_fd(sys.stdout, target), redirect_fd(sys.stderr, sys.stdout):
        yield


_ROOT_LOGGER = object()  # sentinel


@contextmanager
def capture_logs(logger=_ROOT_LOGGER, target=os.devnull, level=None, formatter=None):
    """Context manager that captures python log messages to the specified
    stream for its duration.

    Parameters
    ----------

    target
        A file-like object (compatible with :ref:`logging.StreamHandler`) to
        which messages will be written.
    logger: logging.Logger
        A python logger. If not provided, the root logger is assumed (and thus
        all log messages are captured).
    formatter: logging.Formatter
        A :ref:`logging.Formatter` with which to format the log messages.
    """
    if logger is None or logger is False:
        yield  # do nothing
    else:
        if logger is _ROOT_LOGGER:
            logger = logging.getLogger()
        handler = logging.StreamHandler(target)
        if level is not None:
            handler.setLevel(level)
        if formatter is not None:
            handler.setFormatter(formatter)
        logger.addHandler(handler)
        yield
        logger.removeHandler(handler)


def capture(
    target=os.devnull,
    logger=_ROOT_LOGGER,
    include_stdout=True,
    include_stderr=True,
    loglevel=None,
):
    """Context manager that captures (some subset of) logs, stdout and stderr
    to a single file/stream.

    Parameters
    ----------

    target
        A file-like object (compatible with :ref:`logging.StreamHandler`) to
        which the captured output will be written.
    logger: logging.Logger
        If a :ref:`logging.Logger` is provided, only messages from this logger
        will be captured. Provide ``None`` to disable log capturing. The
        default is to capture the root logger.
    include_stdout: bool
        Whether stdout should be included.
    include_stderr: bool
        Whether stderr should be included.
    loglevel:
        The log level at which to capture messages.
    """
    stdout = redirect_fd(sys.stdout, target) if include_stdout else noop()
    stderr = redirect_fd(sys.stderr, target) if include_stderr else noop()
    logs = capture_logs(logger, target, level=loglevel)
    with stdout, stderr, logs:
        yield
