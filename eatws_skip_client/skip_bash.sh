# Source this from bash.
# These are helper functions designed to provide a workflow framework for
# creating new data products and sending them to the SKIP.

[ -z "$AWS_DEFAULT_REGION" ] && export AWS_DEFAULT_REGION=ap-southeast-2

skip_publish() {
    # Uploads a file to the SKIP, or a stub if the file is missing.
    # $1 is event id
    # $2 is file name
    # remaining args are passed unchanged to eatws-skip-client
    skip product upload -E "$@"
}

skip_download() {
    # Downloads a file from the SKIP.
    # $1 is event id
    # $2 is file name
    skip product download -o $2 -E "$@"
}

skip_create_and_publish() {
    # Runs a script to generate a file, and uploads the resulting file (or a
    # stub) to the SKIP. Captures all output of the script and sends it as the
    # logfile.

    # $1 is event id
    # $2 is filename
    # remaining args are passed unchanged
    # stdin is script to capture output of

    # Example usage:
    # skip_create_and_publish ga2019potato my_new_file.txt <<'END_PRODUCT'
    #     echo "I'm a freshly created product!" > my_new_file.txt
    #     echo "I'm a message that will appear in the log."
    # END_PRODUCT
    logfile=$(mktemp)
    SKIP_EXTRA_ARGS=()

    source /dev/stdin >|"$logfile" 2>&1
    cat "$logfile" >&2
    [ -z "$SKIP_DEBUG" ] || echo "SKIP_EXTRA_ARGS: ${SKIP_EXTRA_ARGS[@]}" >&2
    skip_publish "$@" --logfile "$logfile" "${SKIP_EXTRA_ARGS[@]}"
}
