# eatws-skip-client

This is the python/CLI client for the SKIP. Python 3.8+ is supported.

## Installation

To install the master branch via pip, simply run

```sh
pip install git+https://bitbucket.org/geoscienceaustralia/eatws-skip-client
```

For development, check out this repository and then run

```sh
pip install -e .
```

in your working copy.

## Usage

### Python library

If you're on the GA network, you can view the documentation at
http://docs.gagempa.net/skip-client/. Otherwise, to build the documentation
yourself, install sphinx and then run `make -C docs html`; or simply view the
docstrings in the source code.

Some usage examples:

  * The (WIP) [integration tests](test/integration/)
  * The NEAC's GDS service (look for `ga_skip.py` in the `sc3-infrastructure`
    repo)

### CLI client

The installer should leave you with a script `skip` on your PATH; so you can
access the CLI documentation by running `skip --help`, or e.g. `skip product
upload --help` for detailed help on the `product upload` command.

For a complex example of usage see the NEAC's ShakeMap runner (Look for
`run_shakemap.sh` in `sc3-infrastructure`).
