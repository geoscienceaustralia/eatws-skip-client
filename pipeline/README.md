# Pipeline role

This is a tiny terraform module that deploys the role used by eatws-skip-client's
Bitbucket Pipelines to publish to CodeArtifact.

It should be deployed in the prod account only, with a simple `terraform init
&& terraform apply`.
